from app.database.session import SessionLocal
from app.database.seeders import user_seeder

db = SessionLocal()

try:
    user_seeder.seed_users(db)
finally:
    db.close()
