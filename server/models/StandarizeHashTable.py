from fastapi import HTTPException, status


class StandarizeHashTable:
    def __init__(self, table_size=100):
        self.table_size = table_size
        self.hash_table = [[] for _ in range(table_size)]

    def reinit(self):
        self.hash_table = [[] for _ in range(self.table_size)]

    def _hash_function(self, standarize_id):
        return hash(standarize_id) % self.table_size

    def insert_standarize(self, standarize_id, standarize):
        index = self._hash_function(standarize_id)
        self.hash_table[index].append((standarize_id, standarize))

    def get_standarize_by_id(self, standarize_id):
        index = self._hash_function(standarize_id)
        bucket = self.hash_table[index]
        for doc_id, standarize in bucket:
            if doc_id == standarize_id:
                return standarize
        return None

    def delete_standarize(self, standarize_id):
        index = self._hash_function(standarize_id)
        bucket = self.hash_table[index]

        for i, (doc_id, standarize) in enumerate(bucket):
            if doc_id == standarize_id:
                del bucket[i]
                return

        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Standarize with id {standarize_id} not found')
