from fastapi import HTTPException
from pydantic import BaseModel, validator


class Login(BaseModel):
    username: str
    password: str

    @validator("username", "password", pre=True)
    def validate_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Input not allowed to be empty")
        return value


class CheckLogin(BaseModel):
    auth_key: str

    @validator("auth_key", pre=True)
    def validate_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Input not allowed to be empty")
        return value
