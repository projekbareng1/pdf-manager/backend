from enum import Enum
from fastapi import HTTPException
from pydantic import BaseModel, EmailStr, validator
from typing import List, Optional
import os
import sys

from server.database.session import SessionLocal
from server.models.UserModel import Role, User

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)


class Document(BaseModel):
    title: str
    file_path: Optional[str]


class Role(BaseModel):
    name: str


class RoleEnum(str, Enum):
    admin = "admin"
    pabrik = "pabrik"
    penjualan = "penjualan"


class UserIn(BaseModel):
    name: str
    username: str
    email: EmailStr
    password: str
    role: str

    @validator("name", pre=True)
    def validate_name_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Name input not allowed to be empty")
        return value

    @validator("username", pre=True)
    def validate_username_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Username input not allowed to be empty")
        return value

    @validator("email", pre=True)
    def validate_email_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Email input not allowed to be empty")
        return value

    @validator("password", pre=True)
    def validate_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Password input not allowed to be empty")
        return value

    @validator("username")
    def validate_username_unique(cls, username):
        db = SessionLocal()
        user = db.query(User).filter(User.username == username).first()
        db.close()
        if user:
            raise ValueError("Username already exists")
        return username

    @validator("email")
    def validate_email_unique(cls, email):
        db = SessionLocal()
        user = db.query(User).filter(User.email == email).first()
        db.close()
        if user:
            raise ValueError("Email already exists")
        return email

    @validator("role")
    def validate_role_type(cls, role):
        if role not in ['admin', 'pabrik', 'penjualan']:
            raise ValueError(
                "Invalid fruit value. Allowed values are: 'admin', 'pabrik', 'penjualan'")
        return role


class UserInUpdate(BaseModel):
    name: str
    username: str
    email: EmailStr
    password: Optional[str]
    role: str

    @validator("name", pre=True)
    def validate_name_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Name input not allowed to be empty")
        return value

    @validator("username", pre=True)
    def validate_username_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Username input not allowed to be empty")
        return value

    @validator("email", pre=True)
    def validate_email_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Email input not allowed to be empty")
        return value

    @validator("role")
    def validate_role_type(cls, role):
        if role not in RoleEnum:
            raise ValueError(
                "Invalid fruit value. Allowed values are: 'admin', 'pabrik', 'penjualan'")
        return role


class UserOut(BaseModel):
    id: int
    name: str
    username: str
    email: EmailStr
    documents: List[Document]
    role: Role

    class Config():
        from_attributes = True
        arbitary_types = True
