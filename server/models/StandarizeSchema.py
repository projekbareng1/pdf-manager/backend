from datetime import datetime
from typing import Optional
from pydantic import BaseModel, EmailStr, validator
from fastapi import File, Form, HTTPException, UploadFile


class User(BaseModel):
    id: int
    name: str
    email: EmailStr


class StandarizeIn(BaseModel):
    title: str
    description: str
    target: str
    file: UploadFile

    @validator("title", "description", pre=True)
    def validate_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Input not allowed to be empty")
        return value

    @validator("target")
    def validate_role_type(cls, role):
        if role not in ['admin', 'pabrik', 'penjualan']:
            raise ValueError(
                "Invalid fruit value. Allowed values are: 'admin', 'pabrik', 'penjualan'")
        return role

    @validator("file")
    def validate_files(cls, file):
        if not file.filename.lower().endswith('.pdf'):
            raise HTTPException(
                status_code=422, detail="Only PDF files are allowed")
        return file


class StandarizeInUpdate(BaseModel):
    title: str
    description: str
    target: str
    file: Optional[UploadFile] = File(None)

    @validator("title", "description", pre=True)
    def validate_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Input not allowed to be empty")
        return value

    @validator("target")
    def validate_role_type(cls, role):
        if role not in ['admin', 'pabrik', 'penjualan']:
            raise ValueError(
                "Invalid fruit value. Allowed values are: 'admin', 'pabrik', 'penjualan'")
        return role

    @validator("file")
    def validate_files(cls, file):
        if file and (not file.filename.lower().endswith('.pdf')):
            raise HTTPException(
                status_code=422, detail="Only PDF files are allowed")
        return file


class StandarizeOut(BaseModel):
    id: int
    title: str
    description: str
    target: str
    file_path: Optional[str]
    created_at: datetime
    owner: User

    class Config():
        from_attributes = True
        arbitary_types = True
        json_encoders = {
            datetime: lambda v: v.isoformat()
        }
