from fastapi import HTTPException, status


class DocumentHashTable:
    def __init__(self, table_size=100):
        self.table_size = table_size
        self.hash_table = [[] for _ in range(table_size)]

    def reinit(self):
        self.hash_table = [[] for _ in range(self.table_size)]

    def _hash_function(self, document_id):
        return hash(document_id) % self.table_size

    def insert_document(self, document_id, document):
        index = self._hash_function(document_id)
        self.hash_table[index].append((document_id, document))

    def get_document_by_id(self, document_id):
        index = self._hash_function(document_id)
        bucket = self.hash_table[index]
        for doc_id, document in bucket:
            if doc_id == document_id:
                return document
        return None

    def delete_document(self, document_id):
        index = self._hash_function(document_id)
        bucket = self.hash_table[index]

        for i, (doc_id, document) in enumerate(bucket):
            if doc_id == document_id:
                del bucket[i]
                return

        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Document with id {document_id} not found')
