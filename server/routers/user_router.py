from fastapi import APIRouter, Depends, Query
from sqlalchemy.orm import Session
from typing import List
import os
import sys

from server.database.session import get_db
from server.database.repositories import UserRepository
from server.models.UserSchema import UserIn, UserOut, UserInUpdate
from server.dependencies.oauth2 import get_current_user

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)


router = APIRouter(
    prefix='/user',
    tags=['user']
)


@router.get('/', response_model=List[UserOut])
async def index(db: Session = Depends(get_db), search: str = Query('', alias="search"), page: int = Query(1, alias="page", ge=1), limit: int = Query(10, le=100), current_user: UserOut = Depends(get_current_user)):
    start_index = (page * limit) - limit
    end_index = start_index + limit
    response = UserRepository.get_all_user(db, search, current_user)
    return response


@router.post('/', response_model=UserOut)
async def store(request: UserIn, db: Session = Depends(get_db)):
    user = UserRepository.create_user(request, db)
    return user


@router.get('/{id}/', response_model=UserOut)
async def show(id: int, db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    user = UserRepository.get_user_by_id(db, id, current_user)
    return user


@router.put('/{id}/')
async def update(id: int, request: UserInUpdate, db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    response = UserRepository.update_user_by_id(db, id, request, current_user)
    return response


@router.delete('/{id}/')
async def destroy(id: int, db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    response = UserRepository.delete_user_by_id(db, id, current_user)
    return response
