from pathlib import Path
from fastapi import APIRouter, Depends, HTTPException, Query
from fastapi.responses import FileResponse
from sqlalchemy.orm import Session
from typing import List
import os
import sys

from server.database.session import get_db
from server.dependencies.oauth2 import get_current_user
from server.models.UserSchema import UserOut
from server.models.StandarizeSchema import StandarizeIn, StandarizeInUpdate, StandarizeOut
from server.database.repositories import StandarizeRepository

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)

router = APIRouter(
    prefix='/standarize',
    tags=['standarize']
)


@router.get('/', response_model=List[StandarizeOut])
async def index(db: Session = Depends(get_db), search: str = Query('', alias="search"), page: int = Query(1, alias="page", ge=1), limit: int = Query(10, le=100), current_user: UserOut = Depends(get_current_user)):
    start_index = (page * limit) - limit
    end_index = start_index + limit
    standarizes = StandarizeRepository.get_all_standarize(
        db, search, current_user)
    items = standarizes[start_index:end_index]
    return items


@router.post('/')
async def store(request: StandarizeIn = Depends(), db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    doc = StandarizeRepository.create_standarize(
        db, request, current_user)
    return doc


@router.put('/{id}/')
async def update(id: int, request: StandarizeInUpdate = Depends(), db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    doc = StandarizeRepository.update_standarize_by_id(
        db, id, request, current_user)  # type: ignore
    return doc


@router.get('/{id}/', response_model=StandarizeOut)
async def show(id: int, db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    doc = StandarizeRepository.get_standarize_by_id(db, id, current_user)
    return doc


@router.get('/download/{id}/')
async def download(id: int, db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    doc = StandarizeRepository.get_standarize_by_id(db, id, current_user)
    if (doc):
        filename = doc.title
        file_path = f'./{doc.file_path}'
        if Path(file_path).exists():
            return FileResponse(file_path, filename=f'{filename}.pdf')
        raise HTTPException(
            status_code=404, detail=f'File not found for standarize with id {id}'
        )


@router.delete('/{id}/')
async def destroy(id: int, db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    response = StandarizeRepository.delete_standarize_by_id(
        db, id, current_user)
    return response
