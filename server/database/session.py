import os
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from dotenv import load_dotenv

load_dotenv()

# db_url = os.getenv('DATABASE_URL')

# if db_url is None:
#     raise Exception("DATABASE_URL is not defined in the .env file.")

# SQLALCHEMY_DATABASE_URL = "mysql+mysqlconnector://root:root@mysql:3360/pdf_manager_db"  # db_url
SQLALCHEMY_DATABASE_URL = "postgresql://root:root@postgres/pdf_manager_db"  # db_url

engine = create_engine(
    SQLALCHEMY_DATABASE_URL
)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
