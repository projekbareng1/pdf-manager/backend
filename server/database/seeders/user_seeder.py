from sqlalchemy.orm import Session
from server.dependencies.hash import Hash
from server.models.UserModel import User  # Import your SQLAlchemy model


def seed_users(db: Session):
    sample_data = [
        {"username": "alifwr", "email": "alifwicaksana21@gmail.com",
            "password": Hash.bcrypt('123')},
    ]
    for data in sample_data:
        user = User(**data)
        db.add(user)
    db.commit()
