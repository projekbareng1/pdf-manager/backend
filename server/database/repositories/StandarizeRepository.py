from operator import or_
from fastapi import HTTPException, status
from sqlalchemy.orm.session import Session
from sqlalchemy import or_
import os
import sys

from server.models.StandarizeModel import Standarize
from server.models.StandarizeSchema import StandarizeIn, StandarizeInUpdate, StandarizeOut
from server.models.StandarizeHashTable import StandarizeHashTable
from server.models.UserModel import User
from server.models.UserSchema import UserOut

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)


standarize_hash_table = StandarizeHashTable()


def get_all_standarize(db: Session, search: str, current_user: UserOut):
    base_query = db.query(Standarize)
    if search:
        base_query = base_query.filter(
            or_(
                Standarize.title.ilike(f"%{search}%"),
                Standarize.description.ilike(f"%{search}%"),
            )
        )
    if (current_user.role.name == 'pabrik'):
        base_query = base_query.filter(
            or_(Standarize.target == current_user.role.name, Standarize.target == 'admin'))
        # base_query = base_query.filter_by(target=current_user.role.name)
    elif (current_user.role.name == 'penjualan'):
        base_query = base_query.filter(
            or_(Standarize.target == current_user.role.name, Standarize.target == 'admin'))
        # base_query = base_query.filter_by(target=current_user.role.name)
    standarizes = base_query.all()
    standarize_hash_table.reinit()
    for standarize in standarizes:
        standarize_hash_table.insert_standarize(standarize.id, standarize)
    return standarizes


def create_standarize(db: Session, request: StandarizeIn, current_user: UserOut):
    owner_id = current_user.id
    standarizes = get_all_standarize(db, '', current_user)
    generated_filename = generate_unique_filename(
        request.title, standarizes, owner_id)

    file_path = f'{generated_filename}.pdf'
    os.makedirs(os.path.dirname(file_path), exist_ok=True)
    with open(file_path, "wb") as file_object:
        file_object.write(request.file.file.read())

    new_doc = Standarize(
        title=request.title,
        description=request.description,
        target=request.target,
        owner_id=owner_id,
        file_path=file_path
    )
    db.add(new_doc)
    db.commit()
    db.refresh(new_doc)
    return new_doc


def get_standarize_by_id(db: Session, id: int, current_user: UserOut):
    standarize = standarize_hash_table.get_standarize_by_id(id)
    if standarize:
        return standarize
    standarize = db.query(Standarize).filter_by(id=id).first()
    if not standarize:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Standarize with id {id} not found')
    standarize_hash_table.insert_standarize(standarize.id, standarize)
    return standarize


def update_standarize_by_id(db: Session, standarize_id: int, request: StandarizeInUpdate, current_user: UserOut):
    standarize = db.query(Standarize).filter_by(id=standarize_id)
    owner_id = current_user.id

    standarize.update({
        Standarize.title: request.title,
        Standarize.description: request.description,
        Standarize.target: request.target,
        Standarize.owner_id: owner_id,
    })

    if request.file:
        standarizes = get_all_standarize(db, '', current_user)
        generated_filename = generate_unique_filename(
            request.title, standarizes, owner_id, standarize_id)

        file_path = f'{generated_filename}.pdf'
        with open(file_path, "wb") as file_object:
            file_object.write(request.file.file.read())

        standarize.update({
            Standarize.file_path: file_path
        })

    db.commit()
    standarize = db.query(Standarize).filter_by(id=standarize_id).first()
    if standarize:
        hashed_doc = standarize_hash_table.get_standarize_by_id(standarize.id)
        if hashed_doc:
            standarize_hash_table.delete_standarize(standarize.id)

    return standarize


def generate_unique_filename(title, standarizes, owner_id, id=None):
    save_path = 'public/uploads/'
    registered_filename = set()
    for row in standarizes:
        if (id and id != row.id):
            registered_filename.add(row.file_path.replace('.pdf', ''))
        elif (not id):
            registered_filename.add(row.file_path.replace('.pdf', ''))
    filename = f'{owner_id}_{title}'
    generated_filename = f'{save_path}{filename}'
    if generated_filename in registered_filename:
        idx = 0
        generated_filename = f'{save_path}{filename}_{idx}'
        while generated_filename in registered_filename:
            idx += 1
            generated_filename = f'{save_path}{filename}_{idx}'
    return generated_filename


def delete_standarize_by_id(db: Session, id: int, current_user: UserOut):
    standarize_hash_table.delete_standarize(id)
    standarize = get_standarize_by_id(db, id, current_user)
    if standarize:
        db.delete(standarize)
        db.commit()
        return standarize
    else:
        raise HTTPException(
            status_code=200, detail=f"Standarize with ID {id} not found")
