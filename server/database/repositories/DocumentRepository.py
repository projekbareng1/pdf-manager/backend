from operator import or_
from fastapi import HTTPException, status
from sqlalchemy.orm.session import Session
from sqlalchemy import or_
import os
import sys

from server.models.DocumentModel import Document
from server.models.DocumentSchema import DocumentIn, DocumentInUpdate, DocumentOut
from server.models.DocumentHashTable import DocumentHashTable
from server.models.UserModel import User
from server.models.UserSchema import UserOut

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)


document_hash_table = DocumentHashTable()


def get_all_document(db: Session, search: str, current_user: UserOut):
    base_query = db.query(Document)
    if search:
        base_query = base_query.filter(
            or_(
                Document.title.ilike(f"%{search}%"),
                Document.description.ilike(f"%{search}%"),
            )
        )
    if (current_user.role.name == 'admin'):
        documents = base_query.all()
    else:
        documents = base_query.join(
            User, Document.owner_id == User.id).filter(User.role == current_user.role).all()
    document_hash_table.reinit()
    for document in documents:
        document_hash_table.insert_document(document.id, document)
    return documents


def create_document(db: Session, request: DocumentIn, current_user: UserOut):
    owner_id = current_user.id
    documents = get_all_document(db, '', current_user)
    generated_filename = generate_unique_filename(
        request.title, documents, owner_id)

    file_path = f'{generated_filename}.pdf'
    os.makedirs(os.path.dirname(file_path), exist_ok=True)
    with open(file_path, "wb") as file_object:
        file_object.write(request.file.file.read())

    new_doc = Document(
        title=request.title,
        description=request.description,
        owner_id=owner_id,
        file_path=file_path
    )
    db.add(new_doc)
    db.commit()
    db.refresh(new_doc)
    return new_doc


def get_document_by_id(db: Session, id: int, current_user: UserOut):
    document = document_hash_table.get_document_by_id(id)
    if document:
        return document
    document = db.query(Document).filter_by(id=id).first()
    if not document:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Document with id {id} not found')
    document_hash_table.insert_document(document.id, document)
    return document


def update_document_by_id(db: Session, document_id: int, request: DocumentInUpdate, current_user: UserOut):
    document = db.query(Document).filter_by(id=document_id)
    owner_id = current_user.id

    document.update({
        Document.title: request.title,
        Document.description: request.description,
        Document.owner_id: owner_id,
    })

    if request.file:
        documents = get_all_document(db, '', current_user)
        generated_filename = generate_unique_filename(
            request.title, documents, owner_id, document_id)

        file_path = f'{generated_filename}.pdf'
        with open(file_path, "wb") as file_object:
            file_object.write(request.file.file.read())

        document.update({
            Document.file_path: file_path
        })

    db.commit()
    document = db.query(Document).filter_by(id=document_id).first()
    if document:
        hashed_doc = document_hash_table.get_document_by_id(document.id)
        if hashed_doc:
            document_hash_table.delete_document(document.id)

    return document


def generate_unique_filename(title, documents, owner_id, id=None):
    save_path = 'public/uploads/'
    registered_filename = set()
    for row in documents:
        if (id and id != row.id):
            registered_filename.add(row.file_path.replace('.pdf', ''))
        elif (not id):
            registered_filename.add(row.file_path.replace('.pdf', ''))
    filename = f'{owner_id}_{title}'
    generated_filename = f'{save_path}{filename}'
    if generated_filename in registered_filename:
        idx = 0
        generated_filename = f'{save_path}{filename}_{idx}'
        while generated_filename in registered_filename:
            idx += 1
            generated_filename = f'{save_path}{filename}_{idx}'
    return generated_filename


def delete_document_by_id(db: Session, id: int, current_user: UserOut):
    document_hash_table.delete_document(id)
    document = get_document_by_id(db, id, current_user)
    if document:
        db.delete(document)
        db.commit()
        return document
    else:
        raise HTTPException(
            status_code=200, detail=f"Document with ID {id} not found")
