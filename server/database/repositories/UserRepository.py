from operator import or_
from fastapi import HTTPException, status
from sqlalchemy.orm.session import Session
from sqlalchemy import or_
import os
import sys

from server.models.UserModel import User, Role
from server.models.UserSchema import UserIn, UserInUpdate, UserOut
from server.dependencies.hash import Hash

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)


def get_all_user(db: Session, search: str, current_user: UserOut):
    print(search)
    base_query = db.query(User)
    if search:
        base_query = base_query.filter(
            or_(
                User.name.ilike(f"%{search}%"),
            )
        )
    users = base_query.all()
    return users


def create_user(request: UserIn, db: Session):
    role = db.query(Role).filter_by(name=request.role).first()
    if role is None:
        raise HTTPException(
            status_code=404, detail=f"Role {request.role} not found"
        )
    role_id = role.id

    new_user = User(
        name=request.name,
        username=request.username,
        email=request.email,
        password=Hash.bcrypt(request.password),
        role_id=role_id
    )
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user


def get_user_by_id(db: Session, id: int, current_user: UserOut):
    user = db.query(User).filter_by(id=id).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'User with id {id} not found')
    return user


def get_user_by_username(db: Session, username: str):
    user = db.query(User).filter_by(username=username).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'User with id {id} not found')
    return user


def update_user_by_id(db: Session, id: int, request: UserInUpdate, current_user: UserOut):
    user = get_user_by_id(db, id, current_user)

    users = get_all_user(db, '', current_user)
    registered_emails = set()
    registered_usernames = set()
    for row in users:
        if user.email != row.email:  # type: ignore
            registered_emails.add(row.email)
        if user.username != row.username:  # type: ignore
            registered_usernames.add(row.username)

    if request.email in registered_emails:
        raise HTTPException(
            status_code=422, detail='Email address already registered')

    if request.username in registered_usernames:
        HTTPException(
            status_code=422, detail='Username address already registered')

    user = db.query(User).filter_by(id=id)

    user.update({
        User.name: request.name,
        User.username: request.username,
        User.email: request.email,
    })
    if request.password:
        user.update({
            User.password: Hash.bcrypt(request.password)
        })

    print(user)
    db.commit()
    return user.first()


def delete_user_by_id(db: Session, id: int, current_user: UserOut):
    user = get_user_by_id(db, id, current_user)
    if user:
        db.delete(user)
        db.commit()
        return user
    else:
        raise HTTPException(
            status_code=200, detail=f"Document with ID {id} not found")
